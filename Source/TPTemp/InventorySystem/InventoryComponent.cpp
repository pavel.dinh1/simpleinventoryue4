// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "Items/Item.h"
#include "Engine/Engine.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	Capacity = 20;

}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	for (auto& Item : DefaultItems)
	{
		AddItem(Item);
	}
}

bool UInventoryComponent::AddItem(UItem* Item)
{
	if (Items.Num() >= Capacity || !Item)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Inventory is FULL !"));
		return false;
	}
	else
	{
		Item->OwningInventory = this;
		Item->World = GetWorld();
		Items.Add(Item);

		//Update UI
		OnInventoryUpdated.Broadcast();
		return true;
	}

}

bool UInventoryComponent::RemoveItem(UItem* Item)
{
	if (Item)
	{
		Item->OwningInventory = nullptr;
		Item->World = nullptr;
		Items.RemoveSingle(Item);
		OnInventoryUpdated.Broadcast();

		FActorSpawnParameters SpawnParams;

		UItem* ItemTemp = GetWorld()->SpawnActor<UItem>(Item->StaticClass(), SpawnParams);
		if (ItemTemp)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Item Dropped !"));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Item was lost !"));
		}
		return true;
	}
	return false;
}

