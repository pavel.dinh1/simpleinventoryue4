// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodItem.h"
#include "../../TPTempCharacter.h"

UItem::UItem()
{
	Weight = 1.f;
	ItemDisplayName = FText::FromString("Item");
	UseActionText = FText::FromString("Use");
}

void UFoodItem::UseItem(class ATPTempCharacter* Character)
{
	Super::UseItem(Character);
	if (Character)
	{
		Character->Health += HealAmount;
	}
}