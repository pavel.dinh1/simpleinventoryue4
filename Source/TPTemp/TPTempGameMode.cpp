// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPTempGameMode.h"
#include "TPTempCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPTempGameMode::ATPTempGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
